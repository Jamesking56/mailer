<?php namespace Jamesking56\Mailer;

use Mail;

/**
 * Easy Laravel Mailer trait ready so that you can write cleaner mailers.
 *
 * Created: 04/07/14
 * Editor: PhpStorm
 *
 * @author James King <james@jamesking56.co.uk>
 * @copyright 2014 James King, All Rights Reserved. (http://www.jamesking56.co.uk)
 */
trait Mailer {

    protected function sendEmail(array $to, $subject, array $views, $data = array(), $attachments = array())
    {
        Mail::queue($views, $data, function($message) use($to, $subject, $attachments){
            $message->to($to['email'], $to['name']);
            $message->subject($subject);

            foreach($attachments as $attachment)
            {
                $attachmentData = array();

                if(!empty($attachment['as'])) $attachmentData['as'] = $attachment['as'];
                if(!empty($attachment['mime'])) $attachmentData['mime'] = $attachment['mime'];

                $message->attach($attachment['file'], $attachmentData);
            }
        });
    }

} 
# Jamesking56's Laravel Mailer #

Hey There! Yes, I know its been done before but I thought I would create my own Laravel 4 Mailer trait.

### What features does it have? ###

* Automatically queues emails if a queue is setup in Laravel.
* Easy to install, no service provider needed!

### Usage ###

```
#!php
<?php

class YourAwesomeMailerClass {

    use Jamesking56\Mailer\Mailer;

    public function sendWelcomeEmail($to)
    {
        $to = array(
            'name' => 'John Doe',                   // optional
            'email' => 'john@example.com'
        );
        $subject = "Testing Testing 123";
        $views = array(
            'text' => 'my.text.view',               // Both are optional but at least 1 is required.
            'html' => 'my.html.view'
        );
        $data = array(                              // Optional. You don't have to pass data if you don't want to.
            'user' => Auth::user()
        );
        $attachments = array(                       // Optional. You don't have to specify any attachments if you don't want to.
            array(
                'file' => 'path_to_your_file.jpg',  // File is required if specifying an attachment.
                'as' => 'jepg',                     // Optional. Laravel will make an assumption.
                'mime' => 'image/jpg'               // Optional. Laravel will make an assumption.
            )
        );

        return $this->sendEmail($to, $subject, $views, $data, $attachments);
    }

}
```

For more, please see Laravel's documentation [here](http://laravel.com/docs/mail).

### Contribution guidelines ###

* Feel free to submit a Pull Request. I may however modify your code to document it better or follow my own writing style.

### License ###

* See LICENSE file in the root.